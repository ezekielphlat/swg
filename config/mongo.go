package config

import (
	"os"

	"gopkg.in/mgo.v2"
)

// GetMongoDB is the db instance creator
func GetMongoDB() (*mgo.Database, *mgo.Session, error) {
	host := os.Getenv("MONGO_HOST")

	session, err := mgo.Dial(host)
	if err != nil {
		return nil, nil, err
	}

	db := session.DB("swgEstate")

	return db, session, nil
}
