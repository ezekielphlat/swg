package dao

import (
	"log"

	"bitbucket.org/ezekielphlat/swg/model"
	"bitbucket.org/ezekielphlat/swg/repository"
	"gopkg.in/mgo.v2/bson"
)

//SaveInformation function
func SaveInformation(informationRepository repository.InformationRepository, info *model.Information) (bson.ObjectId, error) {

	if err := informationRepository.Save(info); err == nil {
		return info.ID, nil
	} else {
		return info.ID, err
	}
}

func FindInformations(informationRepository repository.InformationRepository) model.Informations {
	informations, err := informationRepository.FindAll()
	if err != nil {
		log.Println("cannot find Informaitons", err)
	}

	return informations
}
