package dao

import (
	"fmt"
	"log"
	"time"

	"bitbucket.org/ezekielphlat/swg/model"
	"bitbucket.org/ezekielphlat/swg/repository"
)

func SetAppRunCount(appruncountRepository repository.AppRunCountRepository) {
	var arc model.AppRunCount
	arc.ID = 1
	arc.Count = true
	arc.CreatedAt = time.Now()
	arc.UpdatedAt = time.Now()
	err := appruncountRepository.Save(&arc)
	if err != nil {
		log.Println("could not set app run count to true")
	} else {
		fmt.Println("successfully set run count")
	}
}

func HasAppRunned(appruncountRepository repository.AppRunCountRepository) bool {
	arcs, _ := appruncountRepository.FindAll()
	if arcs != nil {
		log.Println(arcs)
		// for _, arc := range arcs {
		// 	// log.Println("count id", arc.ID, arc.Count)

		// }
		return true
	}
	return false

}
func FindGenders(genderRepository repository.GenderRepository) model.Genders {
	genders, err := genderRepository.FindAll()
	if err != nil {
		log.Println("cannot find genders")
	}
	return genders
}

func SaveGender(genderRepository repository.GenderRepository) {

	gs := model.GetGenderDate()

	for _, g := range gs {
		err := genderRepository.Save(&g)
		if err != nil {
			log.Println("cannot save default genders")
		} else {
			log.Println("Gender saved ", g.Name)
		}
	}

}

func FindRoles(roleRepository repository.RoleRepository) model.Roles {
	roles, err := roleRepository.FindAll()
	if err != nil {
		log.Println("cannot find list of roles")
	}
	return roles
}

func SaveRole(roleRepository repository.RoleRepository) {
	rs := model.GetRoleData()
	for _, r := range rs {
		err := roleRepository.Save(&r)
		if err != nil {
			log.Fatal("cannot save default roles")
		} else {
			log.Println("Role saved ", r.Name)
		}
	}
}
