package dao

import (
	"log"

	"bitbucket.org/ezekielphlat/swg/model"
	"bitbucket.org/ezekielphlat/swg/repository"
	"gopkg.in/mgo.v2/bson"
)

//SaveUpload saves new file upload
func SaveUpload(uploadRepository repository.UploadRepository, upload *model.Upload) (bson.ObjectId, error) {
	if err := uploadRepository.Save(upload); err == nil {
		return upload.ID, nil
	} else {
		return upload.ID, err
	}
}

//FindUploads finds all file uploaded
func FindUploads(uploadRepository repository.UploadRepository) model.Uploads {
	uploads, err := uploadRepository.FindAll()
	if err != nil {
		log.Println("cannot find Informaitons", err)
	}

	return uploads
}
