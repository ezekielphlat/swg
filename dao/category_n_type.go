package dao

import (
	"log"

	"bitbucket.org/ezekielphlat/swg/model"
	"bitbucket.org/ezekielphlat/swg/repository"
	"gopkg.in/mgo.v2/bson"
)

//SaveCategory function
func SaveCategory(categoryRepository repository.CategoryRepository, category *model.Category) (bson.ObjectId, error) {

	if err := categoryRepository.Save(category); err == nil {
		return category.ID, nil
	} else {
		return category.ID, err
	}
}

//FindCategories function
func FindCategories(categoryRepository repository.CategoryRepository) model.Categories {
	categories, err := categoryRepository.FindAll()
	if err != nil {
		log.Println("cannot find categories", err)
	}
	if categories == nil {
		return model.Categories{}
	}
	return categories
}

func SaveType(typeRepository repository.TypeRepository, cattype *model.Type) (bson.ObjectId, error) {
	if err := typeRepository.Save(cattype); err == nil {
		return cattype.ID, nil
	} else {
		return cattype.ID, err
	}
}

func FindTypes(typeRepository repository.TypeRepository) model.Types {
	types, err := typeRepository.FindAll()
	if err != nil {
		log.Println("cannot find types", err)
	}
	if types == nil {
		return model.Types{}
	}
	return types
}
func FindTypeById(typeRepository repository.TypeRepository, id bson.ObjectId) *model.Type {
	cattype, err := typeRepository.FindByID(id)
	if err != nil {
		log.Println("cannot find types", err)
	}

	return cattype
}

func FindTypesByCategoryId(typeRepository repository.TypeRepository, id bson.ObjectId) model.Types {
	types, err := typeRepository.FindAllByCatID(id)
	if err != nil {
		log.Println("cannot find types", err)
	}
	if types == nil {
		return model.Types{}
	}
	return types
}
