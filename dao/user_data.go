package dao

import (
	"log"

	"bitbucket.org/ezekielphlat/swg/helper"
	"bitbucket.org/ezekielphlat/swg/model"
	"bitbucket.org/ezekielphlat/swg/repository"
	"gopkg.in/mgo.v2/bson"
)

//Login function is used to login users.
func Login(profileRepository repository.ProfileRepository, email string, password string) (*model.Profile, error) {

	// hashedPassword, err := helper.HashPassword(password)
	// if err != nil {
	// 	log.Println("error hashing password")
	// }
	//TODO: find user by email.
	//TODO: compare the password to the incoming password

	result, err := profileRepository.FindByEmail(email)
	if err != nil {
		return nil, err
	}
	if helper.CheckPasswordHash(password, result.Password) {
		return result, nil
	} else {
		return nil, err
	}
	// switch {
	// case err != nil:
	// 	return nil, err
	// case result == nil:
	// 	return nil, nil
	// }
	// return result, nil
}

//SaveProfile function
func SaveProfile(profileRepository repository.ProfileRepository, profile *model.Profile) (bson.ObjectId, error) {
	hashedPassword, err := helper.HashPassword(profile.Password)
	if err != nil {
		log.Println("error hashing password")
	}

	profile.Password = hashedPassword

	if err := profileRepository.Save(profile); err == nil {
		return profile.ID, nil
	} else {
		return profile.ID, err
	}
}

//FindProfiles function
func FindProfiles(profileRepository repository.ProfileRepository) model.Profiles {
	profiles, err := profileRepository.FindAll()
	if err != nil {
		log.Println("cannot find profiles", err)
	}
	return profiles
}
