import React, { Component } from 'react';

import Header from './public/Header';
import Body from './public/Body';
import Footer from './public/Footer';



export default class PublicHome extends Component {
    render() {
        return (
            <div>

                <Header />
                <Body />
                <Footer />

            </div>
        )
    }
}
