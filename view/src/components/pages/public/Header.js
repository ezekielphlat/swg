import React, { Component } from 'react';
import logo from '../../../img/logo/logo4.png';


export default class Header extends Component {
    render() {
        return (
            <div>
               {/* <!-- header --> */}
	<div className="header wow zoomIn" data-wow-duration="1.5s" data-wow-delay="0.3s">
		<div className="container">
			<div className="logo">
				
				
					<a href="index.html"><i><img src="images/logo4.png" alt="Still Waters Garden Estate logo" /></i>
						{/* <!-- <span>Watch word here....</span> --> */}
					</a>
				
			</div>
			<div className="header-left">
				<nav className="navbar navbar-default">
					{/* <!-- Brand and toggle get grouped for better mobile display --> */}
					<div className="navbar-header">
					  <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span className="sr-only">Toggle navigation</span>
						<span className="icon-bar"></span>
						<span className="icon-bar"></span>
						<span className="icon-bar"></span>
					  </button>
					</div>

					{/* <!-- Collect the nav links, forms, and other content for toggling --> */}
					<div className="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
						
						<nav className="link-effect-9" id="link-effect-9">
							<ul className="nav navbar-nav">
								<li className="active"><a className="hvr-bounce-to-bottom" href="index.html">Home</a></li>
								<li><a href="services.html" className="hvr-bounce-to-bottom">Resitents Information</a></li>
								<li><a href="gallery.html" className="hvr-bounce-to-bottom">Public Event Gallery</a></li>
								 {/* <!-- <li className="dropdown">
									<a href="codes.html" className="dropdown-toggle hvr-bounce-to-bottom" data-hover="Pages" data-toggle="dropdown" aria-expanded="false">Pages <b className="caret"></b></a>
									 <ul className="dropdown-menu">
										<li><a href="icons.html">Font Icons</a></li>
									
										<li><a href="codes.html">Short codes</a></li>
									</ul>
							    </li> --> */}

								<li><a href="contact.html" className="hvr-bounce-to-bottom">Contact</a></li>
							</ul>
						</nav>
					</div>
					{/* <!-- /.navbar-collapse --> */}
				</nav>
			</div>
		</div>
	</div>
{/* <!-- //header --> */}
            </div>
        )
    }
}
