import React, { Component } from 'react';


export default class Body extends Component {
    render() {
        return (
            <div>
                 {/* <!-- banner --> */}
<div className="banner">
	<div className="container">

		<div className="banner-info">
			<div className="col-md-7 banner-left wow flipInY" data-wow-duration="1.5s" data-wow-delay="0s">
				<h3>Welcome To <span> Still Waters Garden Estate </span>  </h3>
				<p>You can still buy or rent an appartment in the estate</p>
				<a className="hvr-outline-out scroll" href="#about">See More About Us</a>
			</div>
			<div className="col-md-5 banner-right wow flipInY" data-wow-duration="1.5s" data-wow-delay="0s">
					<div className="ban-icon ban-col1">
						<img src="images/icon1.png" alt="" />
					</div>
					<div className="ban-icon ban-col2">
						<img src="images/icon2.png" alt="" />
					</div>
					<div className="ban-icon3">
						<img src="images/icon3.png" alt="" />
					</div>
					<div className="ban-icon ban-col1">
						<img src="images/icon3.png" alt="" />
					</div>
					<div className="ban-icon ban-icon2">
						<img src="images/icon3.png" alt="" />
					</div>
				<div className="clearfix"></div>
			</div>
			<div className="clearfix"></div>
		</div>
	</div>
</div>
{/* <!-- //banner -->
<!-- banner-bottom -->
<!---728x90---> */}

<div id="about" className="banner-bottom wow bounceInUp" data-wow-duration="1s" data-wow-delay="0s">
	<div className="container">
		<h3>ABOUT STILL WATERS GARDEN</h3>
		<p className="para1">Still waters garden estate is a peaceful and calm estate... more info needed</p>
		<div className="col-md-4 bottom-grid multi-gd-text">
			
			<a href="gallery.html"><img src="images/pic6.jpg" alt=" "/></a>
		</div>
		<div className="col-md-4 bottom-grid mar-pad">
				<script>
						{/* // You can also use "$(window).load(function() {"
						$(function () {
						 // Slideshow 4
						$("#slider3").responsiveSlides({
							auto: true,
							pager: true,
							nav: false,
							speed: 500,
							namespace: "callbacks",
							before: function () {
						$('.events').append("<li>before event fired.</li>");
						},
						after: function () {
							$('.events').append("<li>after event fired.</li>");
							}
							});
						}); */}
				</script>
			<div  id="top" className="callbacks_container">
				<ul className="rslides" id="slider3">
					<li>
						<h4>Location</h4>
						<p>It is located in a very serene environment.</p>
					</li>
					<li>
						<h4>Brief History</h4>
						<p>Every building in it was built by a reputable construction company.</p>
					</li>
					<li>
						<h4>Side attractions</h4>
						<p>It also has play ground for kids, hall for event and sport field.</p>
					</li>
					<li>
						<h4>Why choose to live here</h4>
						<p>Still waters garden estate is one of the best on the island.</p>
					</li>
				</ul>
			</div>
		</div>
		<div className="col-md-4 bottom-grid multi-gd-text">
			<a href="gallery.html"><img src="images/pic7.jpg" alt=" pics"/></a>
		</div>
		<div className="clearfix"></div>
	</div>
</div>
{/* <!-- //banner-bottom -->
<!---728x90---> */}

{/* <!-- services --> */}
<div className="main_ser w3layouts-agileinfo">
	<div className="container">
		<div className="col-md-7 main_ser_one w3l-agile wow zoomIn" data-wow-duration="1.5s" data-wow-delay="0.1s">
			<h3>Buying and renting houses in the estate</h3>
			<p> It is very easy to buy or rent house in the estate. </p>
			<div className="cont-grids">
				<div className="col-sm-6 cont-grid-one">
					<div className="cont-grid-left wel-grid">
						<div className="btm-clr4">
							<figure className="icon">
								<img src="images/icon1.png" alt=" " />
							</figure>
						</div>
					</div>
					<div className="cont-grid-right">
						<h4>Step 1</h4>
						<p> purchase a form from ........</p>
					</div>
					<div className="clearfix"></div>
				</div>
				<div className="col-sm-6 cont-grid-one">
					<div className="cont-grid-left wel-grid">
						<div className="btm-clr4">
							<figure className="icon">
								<img src="images/icon2.png" alt=" " />
							</figure>
						</div>
					</div>
					<div className="cont-grid-right">
						<h4>Step 2</h4>
						<p> information about the steps to take is needed</p>
					</div>
					<div className="clearfix"></div>
				</div>
				<div className="col-sm-6 cont-grid-one yes_magr">
					<div className="cont-grid-left wel-grid">
						<div className="btm-clr4">
							<figure className="icon">
								<img src="images/icon3.png" alt=" " />
							</figure>
						</div>
					</div>
					<div className="cont-grid-right">
						<h4>Step 3</h4>
						<p> More information about this content is really needed</p>
					</div>
					<div className="clearfix"></div>
				</div>
				<div className="col-sm-6 cont-grid-one yes_magr">
					<div className="cont-grid-left wel-grid">
						<div className="btm-clr4">
							<figure className="icon">
								<img src="images/icon1.png" alt=" " />
							</figure>
						</div>
					</div>
					<div className="cont-grid-right">
						<h4>Step 4</h4>
						<p> We also need the fifth step information</p>
					</div>
					<div className="clearfix"></div>
				</div>
				<div className="clearfix"></div>
			</div>
		</div>
		<div className="col-md-5 main_ser_two w3l-agile wow zoomIn" data-wow-duration="1.5s" data-wow-delay="0.1s">
			<img className="img-responsive" src="images/pic3.jpg" alt=" "/>
			<h4>After following the steps above what next?</h4>
				<div className="accordion">
							<div className="accordion-section">
								<a className="accordion-section-title" href="#accordion-1">Sit Back <i className="glyphicon glyphicon-chevron-down"></i><div className="clearfix"></div>
								</a>
								<div id="accordion-1" className="accordion-section-content">
									<p>I need the content to input here</p>
								</div>
							</div>

							<div className="accordion-section">
								<a className="accordion-section-title" href="#accordion-2">Relax<i className="glyphicon glyphicon-chevron-down"></i><div className="clearfix"></div>
								</a>
								<div id="accordion-2" className="accordion-section-content">
									<p>The content might not have the title "relax"</p>
								</div>
							</div>

							<div className="accordion-section">
								<a className="accordion-section-title" href="#accordion-3">Be patient <i className="glyphicon glyphicon-chevron-down"></i><div className="clearfix"></div>
								</a>
								<div id="accordion-3" className="accordion-section-content">
									<p>I would be the one to input those text accordingly</p>
								</div>
							</div>
							
							
				</div>
				<script>
							{/* jQuery(document).ready(function() {
								function close_accordion_section() {
									jQuery('.accordion .accordion-section-title').removeclassName('active');
									jQuery('.accordion .accordion-section-content').slideUp(300).removeclassName('open');
								}

								jQuery('.accordion-section-title').click(function(e) {
									// Grab current anchor value
									var currentAttrValue = jQuery(this).attr('href');

									if(jQuery(e.target).is('.active')) {
										close_accordion_section();
									}else {
										close_accordion_section();

										// Add active className to section title
										jQuery(this).addclassName('active');
										// Open up the hidden content panel
										jQuery('.accordion ' + currentAttrValue).slideDown(300).addclassName('open'); 
									}

									e.preventDefault();
								});
							}); */}
				</script>

		</div>
		<div className="clearfix"></div>
	</div>
</div>
{/* <!-- //services -->
<!---728x90---> */}

{/* <!-- treatments --> */}
<div className="treatments">
	<h3>Renovations and constructions in the estate</h3>
	<p className="para1">There are still more space in the estate for any new construction projects you can think of </p>
	<div className="col-md-4 treat-grids treat-pad wow bounceInUp" data-wow-duration="1.5s" data-wow-delay="0s">
		<h5>Ongoing Construction work</h5>
		<ul>
			<li><span className="glyphicon glyphicon-arrow-right" aria-hidden="true"></span><a href="#">New Event center</a></li>
			<li><span className="glyphicon glyphicon-arrow-right" aria-hidden="true"></span><a href="#">Children playground</a></li>
			<li><span className="glyphicon glyphicon-arrow-right" aria-hidden="true"></span><a href="#">More Buildings</a></li>
			<li><span className="glyphicon glyphicon-arrow-right" aria-hidden="true"></span><a href="#">The Gate house </a></li>
			<li><span className="glyphicon glyphicon-arrow-right" aria-hidden="true"></span><a href="#">e.t.c</a></li>
		</ul>
	</div>
	<div className="col-md-4 treat-grids grid wow bounceInUp" data-wow-duration="1.5s" data-wow-delay="0.1s">	
		<figure className="effect-apollo">
			<img src="images/pic8.jpg" alt=" "/>
				<figcaption>
					<p>Image caption here</p>
				</figcaption>			
		</figure>
	</div>
	<div className="col-md-4 treat-grids treat-pad wow bounceInUp" data-wow-duration="1.5s" data-wow-delay="0.2s">
		<h5>Existing side attractions</h5>
			<ul>
				<li><span className="glyphicon glyphicon-arrow-right" aria-hidden="true"></span><a href="#">Basketball cult</a></li>
				<li><span className="glyphicon glyphicon-arrow-right" aria-hidden="true"></span><a href="#">Drinks and bar</a></li>
				<li><span className="glyphicon glyphicon-arrow-right" aria-hidden="true"></span><a href="#">Swimming pool</a></li>
				<li><span className="glyphicon glyphicon-arrow-right" aria-hidden="true"></span><a href="#">Football field</a></li>
				<li><span className="glyphicon glyphicon-arrow-right" aria-hidden="true"></span><a href="#">Relaxation centre</a></li>
			</ul>
	</div>
	<div className="col-md-4 treat-grids grid wow bounceInUp" data-wow-duration="1.5s" data-wow-delay="0.3s">	
		<figure className="effect-apollo">
			<img src="images/pic9.jpg" alt=" "/>
				<figcaption>
					<p>Image caption here</p>
				</figcaption>			
		</figure>
	</div>
	<div className="col-md-4 treat-grids treat-head treat_mar wow bounceInUp" data-wow-duration="1.5s" data-wow-delay="0.4s">
		<h4>Our Best Practice for Construction</h4>
		<p>Every construction is according to the comprehensive estate plan on ground.</p>
		<p>And new construction would be closely vetted to fit into the existing plan.</p>
	</div>
	<div className="col-md-4 treat-grids grid treat_mar wow bounceInUp" data-wow-duration="1.5s" data-wow-delay="0.5s">
		<figure className="effect-apollo">
			<img src="images/pic10.jpg" alt=" "/>
				<figcaption>
					<p>Image caption here</p>
				</figcaption>			
		</figure>
	</div>
	<div className="clearfix"></div>
</div>
{/* <!-- //treatments --> */}
            </div>
        )
    }
}
