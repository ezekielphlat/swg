import React, { Component } from 'react';
import adminLogo from '../../../img/logoAdmin.png'
import  {Link} from "react-router-dom";

export default class Header extends Component {
    render() {
        return (
            <div>
                 {/* <!-- Navigation --> */}
                 <nav className="navbar navbar-default navbar-static-top" style={{ marginBottom: 0 }}>
                    <div className="navbar-header">
                        <button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span className="sr-only">Toggle navigation</span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                        </button>
                        <a className="navbar-brand js-scroll-trigger" href="/">
                            <img id="logo" src={adminLogo} alt="Still Waters Garden Estate logo" />
                        </a>

                    </div>
                    {/* <!-- /.navbar-header --> */}

                    <ul className="nav navbar-top-links navbar-right">


                        {/* <!-- /.dropdown --> */}

                        <li className="dropdown">

                            <a className="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i className="fa fa-user fa-fw"></i>
                                <i className="fa fa-caret-down"></i>
                            </a>
                            <ul className="dropdown-menu dropdown-user">

                                <li>
                                    <a href="#">
                                        <h4>Admin</h4>
                                        <i className="fa fa-user fa-fw"></i> View Profile</a>
                                </li>

                                <li className="divider"></li>
                                <li>
                                    <a href="logout">
                                        <i className="fa fa-sign-out fa-fw"></i> Logout</a>
                                </li>
                            </ul>
                            {/* <!-- /.dropdown-user --> */}
                        </li>
                        {/* <!-- /.dropdown --> */}
                    </ul>
                    {/* <!-- /.navbar-top-links --> */}
                    <br />
                    <br />
                    <div className="navbar-info sidebar" role="navigation">
                        <div className="sidebar-nav navbar-collapse">
                            <ul className="nav bg-dark" id="side-menu">
                                <li className="sidebar-search">
                                    <div className="input-group custom-search-form">
                                        <input type="text" className="form-control" placeholder="Search..." />
                                        <span className="input-group-btn">
                                            <button className="btn btn-default" type="button">
                                                <i className="fa fa-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                    {/* <!-- /input-group --> */}
                                </li>
                                <li>
                                    <Link to="/">
                                        <i className="fa fa-dashboard fa-fw"></i> Dashboard</Link>
                                </li>
                                <li>
                                    <a href="#">
                                        <i className="fa fa-info-circle fa-fw"></i> Manage Information
                                        <span className="fa arrow"></span>
                                    </a>
                                    <ul className="nav nav-second-level">
                                        <li>
                                            <Link to="/category">Add New Category</Link>
                                        </li>
                                        <li>
                                            <Link to="/type">Add New Type</Link>
                                        </li>
                                        <li>
                                            <Link to="/Information">Add New Information</Link>
                                        </li>
                                        

                                    </ul>
                                    {/* <!-- /.nav-second-level --> */}
                                </li>
                                <li>
                                    <a href="#">
                                        <i className="fa fa-users fa-fw"></i> Manage User
                                         <span className="fa arrow"></span>
                                    </a>
                                    <ul className="nav nav-second-level">
                                        <li>
                                            <a href="/register">Register New User</a>

                                        </li>
                                        <li>
                                            <a href="#">Edit user information.(comming soon)</a>
                                        </li>

                                    </ul>
                                    {/* <!-- /.nav-second-level --> */}
                                </li>
                                <li>
                                    <a href="#">
                                        <i className="fa fa-file fa-fw"></i> Manage File Upload
                                        <span className="fa arrow"></span>
                                    </a>

                                    <ul className="nav nav-second-level">

                                        <li>
                                            <a href="/upload">Upload New File</a>
                                        </li>
                                        <li>
                                            {/* <!-- for super admin --> */}
                                            <a href="editdoc">Update Old File</a>
                                        </li>

                                    </ul>
                                </li>

                                <li>
                                    <a href="tables.html">
                                        <i className="fa fa-edit fa-fw"></i> Requisition
                                        <span className="fa arrow"></span>
                                    </a>

                                    <ul className="nav nav-second-level">
                                        <li>
                                            {/* <!-- for super admin --> */}
                                            <a href="request.html">Request</a>
                                        </li>

                                    </ul>
                                </li>

                                <li>
                                    <a href="tables.html">
                                        <i className="fa fa-credit-card fa-fw"></i> Finance
                                        <span className="fa arrow"></span>
                                    </a>

                                    <ul className="nav nav-second-level">
                                        <li>
                                            {/* <!-- for super admin --> */}
                                            <a href="dues.html">Estate Dues</a>
                                        </li>

                                    </ul>
                                </li>


                            </ul>
                        </div>
                        {/* <!-- /.sidebar-collapse --> */}
                    </div>
                    {/* <!-- /.navbar-static-side --> */}

                    

                </nav>
            </div>
        )
    }
}
