import React, { Component } from 'react'
import axios from 'axios'
import { baseUrl } from '../../Helper'
import 'jodit';
import 'jodit/build/jodit.min.css';
import JoditEditor from "jodit-react";

export default class InformationForm extends Component {
    constructor() {
        super()
        this.state = {
            categories: [],
            types: [],
            category: '',
            type: '',
            information: 'content'

        }
    }
    componentWillMount() {
        this.populateCategoryList()
    }
   
    /**
     * @property Jodit jodit instance of native Jodit
     */
    jodit;
    setRef = jodit => this.jodit = jodit;

    config = {
        readonly: false, // all options from https://xdsoft.net/jodit/doc/
        height: 700
    }
    handleCategoryChange = (event) => {
        let catId = event.target.value
        this.setState({ category: catId })
        if(catId){
            this.populateTypeList(catId)

        }else{
            this.setState({types:[]})
        }


    }
    handleOtherChanges = (event) => {
        const target = event.target
        const value = target.value;
        const name = target.name;
        this.setState({ [name]: value })
    }
    handleEditorChange = value => {
        this.setState({ information: value })

    }
    populateTypeList = (category) => {
        
            axios.get(baseUrl + '/category_n_type/types/' + category).then(res => {
                
                    const types = res.data

                    this.setState({ types })
                
                //   console.log(this.state.categories)
            })
        
    }
    populateCategoryList = () => {
        axios.get(baseUrl + '/category_n_type/categories').then(res => {
            const categories = res.data
            this.setState({ categories })
            //   console.log(this.state.categories)
        })
    }
    handleSubmit = event => {
        event.preventDefault()
        var infoData = new FormData()
        infoData.set('category', this.state.category)
        infoData.set('type', this.state.type)
        infoData.set('info', this.state.information)
       
        axios({
            method: 'post',
            url: baseUrl+'/information/addinfo',
            data: infoData
      }).then(res => {
        
        console.log(res.data)
        this.setState({
            category: '',
            type: '',
            information: ''

    })
      }).catch(err=>{
          console.log(err)
      })
    }
    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit} id="" className="form-horizontal">
                    <div className="form-body">
                        <div className="form-group ">
                            <label className="control-label col-md-2">
                                Select Category
                    </label>
                            <div className="col-md-4">
                                <select name="category" value={this.state.category} onChange={this.handleCategoryChange} className="form-control">
                                    <option value="-1">Select a Category</option>
                                    {this.state.categories.map(category =>
                                        <option key={category.id} value={category.id}>{category.name}</option>
                                    )}


                                </select>
                            </div>
                        </div>
                        <div className="form-group ">
                            <label className="control-label col-md-2">Select Type</label>
                            <div className="col-md-4">
                                <select value={this.state.type} onChange={this.handleOtherChanges} name="type" className="form-control">
                                    <option value="-1">Select a Type</option>
                                    {this.state.types.map(type =>
                                        <option key={type.id} value={type.id}>{type.name}</option>
                                    )}

                                </select>
                            </div>
                        </div>

                        <div className="form-group">
                            <label className="control-label col-md-2">Info Editor</label>
                            <div className="col-md-10 fr-view">
                                {/* <div>{this.state.information}</div> */}
                                <JoditEditor
                                    editorRef={this.setRef}
                                    value={this.state.information}
                                    config={this.config}
                                    onChange={this.handleEditorChange}
                                    name="info"
                                />
                            </div>
                        </div>
                    </div>
                    <div className="form-actions">
                        <div className="row">
                            <div className="col-md-offset-3 col-md-9">
                                <button type="submit" className="btn green">
                                    POST
                      </button>
                                <button type="button" className="btn default">
                                    Cancel
                      </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}
