import React, { Component } from "react";
import RegistrationForm from './RegistrationForm'

export default class Register extends Component {
  render() {
    return (
      <div>
        <div className="row">
          <div className="col-lg-12">
            <h2 className="page-header">User Registration</h2>
          </div>
          {/* <!-- /.col-lg-12 --> */}
          {/* <!--Start FORM--> */}
          <div>
            <div className="row">
              <div className="col-md-12">
                {/* <!-- BEGIN VALIDATION STATES--> */}
                <div className="portlet light portlet-fit portlet-form bordered">
                  <div className="portlet-title">
                    <div className="caption">
                      <i className="icon-bubble font-green" />
                      <span className="caption-subject font-green bold uppercase">
                        Kindly fill in your details correctly
                      </span>
                    </div>
                  </div>
                  <div className="portlet-body">
                    {/* <!-- BEGIN FORM--> */}

                    <div> error Message</div>

                   <RegistrationForm/>
                  </div>
                </div>
                {/* // <!-- END VALIDATION STATES--> */}
              </div>
            </div>
            <div className="row">
              <div className="col-md-12" />
              {/* <!--END FORM--> */}
            </div>
            {/* // <!-- /.row --> */}
          </div>
          {/* // <!-- /#page-wrapper --> */}
          <br />
          <br />
        </div>
      </div>
    );
  }
}
