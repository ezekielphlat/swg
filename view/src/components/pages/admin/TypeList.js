import React, { Component } from 'react'
import axios from 'axios'
import { baseUrl } from '../../Helper'
import TypeForm from './TypeForm'


export default class TypeList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            types: [],
            category: '',
            showSave: false,
            showEdit: true
        }
    }
    componentDidMount() {
        // this.setState({category: this.props.cat})
        // this.populateTypeList(this.props.cat)
    }
    componentWillUpdate(nextProps, nextState){
        if(nextProps.cat === this.props.cat){
           
            return false
        }
        // this.setState({category: nextProps.cat})

        
        // console.log("category",this.state.category)

        this.populateTypeList(nextProps.cat)
        console.log(nextProps.cat)
        console.log(this.props.cat)
        
        return true
        
    }
    
    populateTypeList = (category) => {
        
            axios.get(baseUrl + '/category_n_type/types/' + category).then(res => {
                const types = res.data
                this.setState({ types })
                //   console.log(this.state.categories)
            })
        
    }
    render() {
        return (
            <div>
                <div className="row">
                    <div className="col-lg-10">
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                Types of Estate Information
                                
                    </div>
                            {/* <!-- /.panel-heading --> */}
                            <div className="panel-body">
                                <div className="table-responsive">
                                    <table className="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>s/n</th>
                                                <th>Name</th>
                                                <th>Actions</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.state.types.map((type, i) =>
                                                <tr key={type.id}>
                                                    <td>{i + 1}</td>
                                                    <td>{type.name}</td>
                                                    <td>
                                                        {
                                                            this.state.showEdit &&
                                                            <button id={type.id} className="btn btn-primary">Edit</button>

                                                        }{this.state.showSave &&
                                                            <button id={type.id} className="btn btn-info">Save</button>
                                                        }
                                                        <button id={type.id} className="btn btn-danger">Delete</button>
                                                    </td>

                                                </tr>
                                            )}



                                        </tbody>
                                    </table>
                                </div>
                                {/* <!-- /.table-responsive --> */}
                            </div>
                            {/* <!-- /.panel-body --> */}
                        </div>
                        {/* <!-- /.panel --> */}
                    </div>
                    {/* <!-- /.col-lg-6 --> */}
                </div>
                <TypeForm populateType={this.populateTypeList} cat={this.props.cat?this.props.cat : ""}/>

            </div>
        )
    }
}
