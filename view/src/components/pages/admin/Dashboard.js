import React, { Component } from 'react'

export default class Dashboard extends Component {
    render() {
        return (
            <div>


                
                    <div className="row">
                        <div className="col-lg-12">
                            <h1 className="page-header">Dashboard</h1>

                        </div>
                        {/* <!-- /.col-lg-12 --> */}
                    </div>
                    {/* <!-- /.row --> */}

                    <div className="row">
                        <div className="col-lg-10">
                            <div className="panel panel-default">

                                {/* <!-- /.panel-heading --> */}
                                <div className="panel-body">
                                    <div id="accordion">
                                        <div className="card">
                                            <div className="card-header" id="headingOne">
                                                <h5 className="mb-0 text-center">
                                                    <a className="text-capitalized text-center text-info category-icon" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                                                        aria-controls="collapseOne">
                                                        <i className="fa fa-info-circle fa-4x"></i>
                                                        <h3>Estate Information</h3>

                                                    </a>
                                                </h5>
                                            </div>

                                            <div id="collapseOne" className="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                                <div className="card-body">
                                                    <div className="list-group">
                                                        <a href="#" className="list-group-item list-group-item-action">
                                                            By Laws
                                        </a>
                                                        <a href="#" className="list-group-item list-group-item-action">Committees</a>
                                                        <a href="#" className="list-group-item list-group-item-action">Morbi leo risus</a>
                                                        <a href="#" className="list-group-item list-group-item-action">Porta ac consectetur ac</a>
                                                        {/* <!-- <a href="#" className="list-group-item list-group-item-action disabled">Vestibulum at eros</a> --> */}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="card">
                                            <div className="card-header" id="headingTwo">
                                                <h5 className="mb-0 text-center">
                                                    <a className="text-capitalized text-center text-info collapsed category-icon" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false"
                                                        aria-controls="collapseTwo">
                                                        <i className="fa fa-file fa-4x"></i>
                                                        <h3>Files & Document Storage</h3>

                                                    </a>
                                                </h5>
                                            </div>

                                            <div id="collapseTwo" className="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                                                <div className="card-body">
                                                    <div className="list-group">
                                                        <a href="#" className="list-group-item list-group-item-action">
                                                            By Laws
                                        </a>
                                                        <a href="#" className="list-group-item list-group-item-action">Committees</a>
                                                        <a href="#" className="list-group-item list-group-item-action">Morbi leo risus</a>
                                                        <a href="#" className="list-group-item list-group-item-action">Porta ac consectetur ac</a>
                                                        {/* <!-- <a href="#" className="list-group-item list-group-item-action disabled">Vestibulum at eros</a> --> */}
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="card">
                                                <div className="card-header" id="headingThree">
                                                    <h5 className="mb-0 text-center">
                                                        <a className="text-capitalized text-center text-info collapsed category-icon" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false"
                                                            aria-controls="collapseThree">
                                                            <i className="fa fa-edit fa-4x"></i>
                                                            <h3>Requisitions</h3>

                                                        </a>
                                                    </h5>
                                                </div>

                                                <div id="collapseThree" className="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                                                    <div className="card-body">
                                                        <div className="list-group">
                                                            <a href="#" className="list-group-item list-group-item-action">
                                                                By Laws
                                            </a>
                                                            <a href="#" className="list-group-item list-group-item-action">Committees</a>
                                                            <a href="#" className="list-group-item list-group-item-action">Morbi leo risus</a>
                                                            <a href="#" className="list-group-item list-group-item-action">Porta ac consectetur ac</a>
                                                            {/* <!-- <a href="#" className="list-group-item list-group-item-action disabled">Vestibulum at eros</a> --> */}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="card">
                                                    <div className="card-header" id="headingFour">
                                                        <h5 className="mb-0 text-center">
                                                            <a className="text-capitalized text-center text-info collapsed catgory-icon" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false"
                                                                aria-controls="collapseFour">
                                                                <i className="fa fa-credit-card fa-4x"></i>
                                                                <h3>Finance</h3>

                                                            </a>
                                                        </h5>
                                                    </div>

                                                    <div id="collapseFour" className="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                                                        <div className="card-body">
                                                            <div className="list-group">
                                                                <a href="#" className="list-group-item list-group-item-action">
                                                                    By Laws
                                                </a>
                                                                <a href="#" className="list-group-item list-group-item-action">Committees</a>
                                                                <a href="#" className="list-group-item list-group-item-action">Morbi leo risus</a>
                                                                <a href="#" className="list-group-item list-group-item-action">Porta ac consectetur ac</a>
                                                                {/* <!-- <a href="#" className="list-group-item list-group-item-action disabled">Vestibulum at eros</a> --> */}
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                            {/* <!-- /.panel-body --> */}
                                        </div>
                                        {/* <!-- /.panel --> */}
                                    </div>
                                    {/* <!-- /.col-lg-6 --> */}
                                </div>
                                {/* <!-- /#row --> */}

                            </div>
                            {/* <!-- /#page-wrapper --> */}
                            <br />
                            <br />
                        </div>
                        </div>
                        

                    </div>
                    )
                }
            }
