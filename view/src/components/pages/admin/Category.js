import React, { Component } from 'react'
import CategoryForm from './CategoryForm'
import CategoryList from './CategoryList'
import {baseUrl} from '../../Helper'
import axios from 'axios'

export default class Category extends Component {
    constructor(){
        super()
        this.state = {
            categories:[],
            
        }
    }
    componentWillMount(){
        this.populateCategoryList()
    }
    
   
    populateCategoryList=()=>{
        axios.get(baseUrl+'/category_n_type/categories').then(res =>{
            const categories = res.data
            this.setState({categories})
        //   console.log(this.state.categories)
          })
    }
    render() {
        return (
            <div>



                <div className="row">
                    <div className="col-lg-12">
                        <h4 className="page-header">Categories</h4>
                    </div>


                </div>
                {/* <!-- #/row --> */}
                <div className="row">
                    <div className="col-lg-10">
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                Category List
                </div>
                            {/* <!-- /.panel-heading --> */}
                           <CategoryList cats={this.state.categories}/>
                            
                            {/* <!-- /.panel-body --> */}
                        </div>
                        {/* <!-- /.panel --> */}
                    </div>
                    {/* <!-- /.col-lg-6 --> */}
                </div>
                {/* <!-- /#row --> */}
               <CategoryForm populateCatList={this.populateCategoryList} />
                {/* <!-- #/row --> */}
                
              
                <br/>
                    <br/>

            
            </div>
                    )
                }
            }
