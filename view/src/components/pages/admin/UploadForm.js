import React, { Component } from 'react'
import axios from 'axios'
import { baseUrl } from '../../Helper'

export default class UploadForm extends Component {
    constructor() {
        super()
        this.state = {
            categories: [],
            types: [],
            category: '',
            type: '',
            title: '',
            file:null,
            description: ''

        }
    }
    componentWillMount() {
        this.populateCategoryList()
    }
    populateCategoryList = () => {
        axios.get(baseUrl + '/category_n_type/categories').then(res => {
            const categories = res.data
            this.setState({ categories })
            //   console.log(this.state.categories)
        })
    }
    handleCategoryChange = (event) => {
        let catId = event.target.value
        this.setState({ category: catId })
        if (catId) {
            this.populateTypeList(catId)

        } else {
            this.setState({ types: [] })
        }


    }
    handleSubmit = event =>{
        event.preventDefault()
        // console.log(this.state.file)
        var uploadFormData = new FormData()
        uploadFormData.set("category", this.state.category)
        uploadFormData.set("type", this.state.type)
        uploadFormData.set("title", this.state.title)
        uploadFormData.set("file", this.state.file)
        uploadFormData.set("description", this.state.description)

        console.log(uploadFormData.entries())
        

        axios({
            method: 'post',
            url: baseUrl+'/upload/addnew',
            data: uploadFormData
      }).then(res => {
        
        console.log(res.data)
        this.setState({
            category: '',
            type: '',
            title: '',
            file:null,
            description: ''

    })
      }).catch(err=>{
          console.log(err)
      })
    }
    handleOtherChanges = (event) => {
        const target = event.target
        const value = target.value;
        const name = target.name;
        this.setState({ [name]: value })
    }
    handleFileChange = event =>{
        event.preventDefault()
        this.setState({file: event.target.files[0]})
    }
    populateTypeList = (category) => {

        axios.get(baseUrl + '/category_n_type/types/' + category).then(res => {

            const types = res.data

            this.setState({ types })

            //   console.log(this.state.categories)
        })

    }
  render() {
    return (
      <div>
         <form onSubmit={this.handleSubmit} className="form-horizontal">
                                <div className="form-body">
                                    <div className="form-group ">
                                        <label className="control-label col-md-2">Select Category
                                </label>
                                        <div className="col-md-4">
                                            <select name="category" value={this.state.category} onChange={this.handleCategoryChange} className="form-control">
                                                <option value="-1">Select a Category</option>
                                                {this.state.categories.map(category =>
                                                    <option key={category.id} value={category.id}>{category.name}</option>
                                                )}


                                            </select>

                                        </div>


                                    </div>
                                    <div className="form-group ">
                                        <label className="control-label col-md-2">Select Type
                                </label>
                                        <div className="col-md-4">
                                            <select value={this.state.type} onChange={this.handleOtherChanges} name="type" className="form-control">
                                                <option value="-1">Select a Type</option>
                                                {this.state.types.map(type =>
                                                    <option key={type.id} value={type.id}>{type.name}</option>
                                                )}

                                            </select>

                                        </div>


                                    </div>
                                    <div className="form-group">
                                        <label className="control-label col-md-2">Document title
        
                                </label>
                                        <div className="col-md-4">
                                            <input 
                                            type="text" 
                                            name="title" 
                                            value={this.state.title} 
                                            onChange={this.handleOtherChanges} 
                                            className=" form-control" />
                                        </div>
                                    </div>
                                    <div className="form-group ">
                                        <label className="control-label col-md-2">Upload File
                                </label>
                                        <div className="col-md-4">
                                            <input name="file" onChange={this.handleFileChange} type="file" />

                                        </div>


                                    </div>



                                    <div className="form-group">
                                        <label className="control-label col-md-2">Description
        
                                </label>
                                        <div className="col-md-6">
                                            <textarea name="description" value={this.state.description} onChange={this.handleOtherChanges} className=" form-control" rows="6" ></textarea>

                                        </div>
                                    </div>
                                </div>
                                <div className="form-actions">
                                    <div className="row">
                                        <div className="col-md-offset-3 col-md-9">
                                            <div className="col-xs-3">
                                                <button type="submit" className="btn btn-info">POST</button>
                                            </div>
                                            <div className="col-xs-3">
                                                <button type="button" className="btn btn-default">Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
      </div>
    )
  }
}
