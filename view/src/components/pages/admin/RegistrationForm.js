import React, { Component } from 'react'
import axios from 'axios'
import {baseUrl} from '../../Helper'



export default class RegistrationForm extends Component {
  
    constructor(){
        super()
        this.state={
          firstname: '',
          lastname :'',
          role :'',
          gender :'',
          email :'',
          password :'',
          roles: [],
        }
    }
    componentWillMount(){
     

      axios.get(baseUrl+'/profile/roles').then(res =>{
        const roles = res.data
        this.setState({roles})
        // console.log(roles)
      console.log(this.state.roles)
      })
    }
    
    handleChange = event =>{
      const target = event.target
     const value = target.type === 'checkbox' ? target.checked : target.value;
      const name = target.name;
      this.setState({[name]:value})
    }

      onSubmit = event =>{
        event.preventDefault()
        var userDate  = new FormData();
        userDate.set('firstname',this.state.firstname)
        userDate.set('lastname',this.state.lastname)
        userDate.set('roleId',this.state.role)
        userDate.set('genderId',this.state.gender)
        userDate.set('email',this.state.email)
        userDate.set('password',this.state.password)
        
        axios({
              method: 'post',
              url: baseUrl+'/profile/register',
              data: userDate
        }).then(res => {
          console.log(res)
          console.log(res.data)
        })
        // console.log(this.state)
      }

    render() {
        return (
            <div>
                 <form
                      action="#"
                      id="form_sample_2"
                      onSubmit={this.onSubmit}
                      className="form-horizontal"
                    >
                      <div className="form-body">
                        <div className="form-group  margin-top-20">
                          <label className="control-label col-md-3">
                            FirstName
                          </label>
                          <div className="col-md-4">
                            <div className="input-icon right">
                              <i className="fa" />
                              <input
                                type="text"
                                className="form-control"
                                value={this.state.firstname}
                                onChange={this.handleChange}
                                name="firstname"
                              />
                            </div>
                          </div>
                        </div>

                        <div className="form-group  margin-top-20">
                          <label className="control-label col-md-3">LastName</label>
                          <div className="col-md-4">
                            <div className="input-icon right">
                              <i className="fa" />
                              <input
                                type="text"
                                className="form-control"
                                value={this.state.lastname}
                                onChange={this.handleChange}
                                name="lastname"
                              />
                            </div>
                          </div>
                        </div>
                        <div className="form-group  margin-top-20">
                          <label className="control-label col-md-3">Role</label>
                          <div className="col-md-4">
                            <div className="input-icon right">
                              <select value={this.state.role} onChange={this.handleChange} className="form-control" name="roleId">
                                <option>Choose...</option>
                                {
                                  this.state.roles.map(role =><option key={role.id} value={role.id}>{role.name}</option> )
                                }
                                
                              </select>
                            </div>
                          </div>
                        </div>
                        <div className="form-group">
                          <label className="control-label col-md-3">Gender</label>
                          <div className="col-md-4">
                            <div
                              className="mt-radio-list"
                              data-error-container="#form_2_membership_error"
                            >
                              <label className="mt-radio">

                                <input type="radio" onChange={this.handleChange} value="1" name="gender" checked={this.state.gender === "1"} /> Male
                                
                              </label>
                              <label className="mt-radio">
                                <input type="radio" onChange={this.handleChange} value="2" name="gender" checked={this.state.gender === "2"} /> Female
                               
                              </label>
                            </div>
                            <div id="form_2_membership_error"> </div>
                          </div>
                        </div>
                        <div className="form-group">
                          <label className="control-label col-md-3">Email</label>
                          <div className="col-md-4">
                            <div className="input-icon right">
                              <i className="fa" />
                              <input
                                type="text"
                                value={this.state.email}
                                onChange={this.handleChange}
                                className="form-control"
                                name="email"
                              />
                            </div>
                            <span className="help-block">
                              
                              e.g: stillwaters@yahoo.com
                            </span>
                          </div>
                        </div>

                        <div className="form-group">
                          <label className="control-label col-md-3">Password</label>
                          <div className="col-md-4">
                            <div className="input-icon right">
                              <i className="fa" />
                              <input
                                type="password"
                                value={this.state.password}
                                onChange={this.handleChange}
                                className="form-control"
                                name="password"
                              />
                            </div>
                          </div>
                        </div>
                        <div className="form-group">
                          <label className="control-label col-md-3">
                            Confirm Password
                          </label>
                          <div className="col-md-4">
                            <div className="input-icon right">
                              <i className="fa" />
                              <input
                                type="password"
                                className="form-control"
                                name="confirmpassword"
                                value={this.state.confirmPassword}
                                onChange={this.handleChange}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="form-actions">
                        <div className="row">
                          <div className="col-md-offset-3 col-md-9">
                          <div className="col-md-3">
                            <button type="submit" className="btn btn-info">
                              Submit
                            </button>
                            </div>
                            <div className="col-md-3">
                            <button type="button" className="btn default">
                              Cancel
                            </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </form>
                    {/* // <!-- END FORM--> */}
            </div>
        )
    }
}
