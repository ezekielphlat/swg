import React, { Component } from 'react'
import axios from 'axios';
import {baseUrl} from '../../Helper'


export default class CategoryForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            category: '',
            

        }
    }

    handleChange = event => {
        const target = event.target
        const value = target.value;
        const name = target.name;
        this.setState({ [name]: value })
    }
    handleSubmit=event=>{
        event.preventDefault()
        var categoryData = new FormData()
        categoryData.set('name', this.state.category)
        axios({
            method: 'post',
            url: baseUrl+'/category_n_type/category',
            data: categoryData
      }).then(res => {
        this.props.populateCatList()
        console.log(res.data)
        this.setState({category: ''})
      }).catch(err=>{
          console.log(err)
      })
    }
    render() {
        
        return (
            <div className="row">
                <div className="container col-lg-10">

                    {/* <!-- BEGIN FORM--> */}
                    <form action="#" onSubmit={this.handleSubmit} id="form_sample_2" className="form-horizontal">
                        <div className="form-body">

                            <div className="form-group  margin-top-20">
                               
                                    <label className="control-label col-md-3">Enter New Category
                                                </label>
                                    <div className="col-md-4">
                                        <div className="input-icon right">

                                            <input type="text" value={this.state.category} onChange={this.handleChange} className="form-control" name="category" />
                                            {/* <span>{this.state.category}</span> */}
                                        </div>
                                    </div>
                                    <div className="col-md-2 text-center">
                                        <button type="submit" className="btn btn-info">Add</button>


                                    </div>
                               
                            </div>


                        </div>

                    </form>
                </div>
            </div>
        )
    }
}
