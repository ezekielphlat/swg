import React, { Component } from 'react'
import axios from 'axios'
import {baseUrl} from '../../Helper'
import TypeList from './TypeList';


export default class Type extends Component {
    constructor(){
        super()
        this.state={
            categories:[],
            types:[],
            type:'',
            category:''
        }
    }
    componentWillMount(){
        this.populateCategoryList()
    }
    handleCategoryChange=(event)=>{
        // const cat = event.target.value
        this.setState({category: event.target.value})
        // this.populateTypeList()


    }
   
    populateCategoryList=()=>{
        axios.get(baseUrl+'/category_n_type/categories').then(res =>{
            this.setState({categories: res.data})
        //   console.log(this.state.categories)
          })
    }

    render() {
        
        return (
            <div>


                
                    <div className="row">
                        <div className="col-lg-12">
                            <h4 className="page-header">Types</h4>
                        </div>


                    </div>
                    <div className="row">
                        <div className="col-lg-10">
                            <div className="panel-body">
                                <div className="form-body">

                                    <div className="form-group ">
                                        <label className="control-label col-md-3">Select Category
                        </label>
                                        <div className="col-md-4">
                                            <select onChange={this.handleCategoryChange} value={this.state.category} className="form-control">
                                                <option value="-1">Select a Category</option>
                                                {this.state.categories.map(category =>
                                                <option key={category.id} value={category.id}>{category.name}</option>
                                            )}
                                                

                                            </select>
                                            <span>{this.state.category}</span>

                                        </div>


                                    </div>
                                </div>


                            </div>
                        </div>
                        {/* <!-- #/row --> */}
                      
                       <TypeList  cat={this.state.category? this.state.category: ''}/>
                      
                        {/* <!-- /#row --> */}
                        {/* <!-- #/row --> */}
                    </div>
                    {/* <!-- /#page-wrapper --> */}
                    {/* <!-- /.row --> */}

               
                <br/>
                    <br/>

            </div>
                    )
                }
            }
