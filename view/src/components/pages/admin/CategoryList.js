import React, { Component } from 'react'
import axios from 'axios';
import {baseUrl} from '../../Helper'

export default class CategoryList extends Component {
    constructor(props){
        super(props)
        this.state = {
          
            showSave: false,
            showEdit: true
        }
    }
    
    render() {
        return (
            <div className="panel-body">
            <div className="table-responsive">
                <table className="table table-striped table-bordered table-hover">
                    <thead>

                        <tr>
                            <th>s/n</th>
                            <th>Name</th>
                            <th>Actions</th>

                        </tr>
                    </thead>
                    <tbody>
                       
                        {this.props.cats.map((category, i) => 
                        <tr key={category.id}>
                            <td>{i+1}</td>
                            <td>{category.name}</td>
                            <td>
                                {
                                    this.state.showEdit &&
                                <button id={category.id} className="btn btn-primary">Edit</button>
                               
                                }{this.state.showSave &&
                                <button id={category.id} className="btn btn-info">Save</button>
                               }
                                <button id={category.id} className="btn btn-danger">Delete</button>
                            </td>

                        </tr>
                        )}
                       

                    </tbody>
                </table>
            </div>
            </div>
        )
    }
}
