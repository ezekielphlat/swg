import React, { Component } from 'react'
import axios from 'axios'
import {baseUrl} from '../../Helper'

export default class TypeForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            type: '',
            

        }
    }

    handleChange = event => {
        const target = event.target
        const value = target.value;
        const name = target.name;
        this.setState({ [name]: value })
    }
    handleSubmit=event=>{
        event.preventDefault()
        var typeData = new FormData()
        typeData.set('name', this.state.type)
        typeData.set('category', this.props.cat)
        // console.log(typeData)
        if (this.props.cat) {
            axios({
                method: 'post',
                url: baseUrl+'/category_n_type/type',
                data: typeData
          }).then(res => {
            this.props.populateType(this.props.cat)
            console.log(res.data)
            this.setState({type: ''})
          }).catch(err=>{
              console.log(err)
          })
        }
        
    }
    render() {
        return (
            <div className="row">
                            <div className="container col-lg-10">

                                {/* <!-- BEGIN FORM--> */}
                <form onSubmit={this.handleSubmit} id="form_sample_2" className="form-horizontal">
                                    <div className="form-body">

                                        <div className="form-group  margin-top-20">
                                            <label className="control-label col-md-3">Enter New Type
                                                                     </label>
                                            <div className="col-md-4">
                                                <div className="input-icon right">
                                                    <input type="hidden" value={ this.props.cat} name="category"/>
                                                    <input type="text" onChange={this.handleChange} value={this.state.type} className="form-control" name="type" />
                                                     </div>
                                            </div>
                                            <div className="col-md-2 text-center">
                                                <button type="submit" className="btn btn-info">Add</button>


                                            </div>
                                        </div>


                                    </div>

                                </form>
                            </div>
                        </div>
        )
    }
}
