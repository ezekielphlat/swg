import React, { Component } from "react";
import InformationForm from "./InformationForm";

export default class Information extends Component {

  render() {
    return (
      <div>
        <div className="row">
          <div className="col-lg-12">
            <h1 className="page-header">New Information </h1>
          </div>
          {/* <!-- /.col-lg-12 --> */}
        </div>
        {/* <!-- /.row --> */}
        <div className="row">
          <div className="col-lg-12">
            <div className="panel-body">
              {/* <!-- BEGIN FORM--> */}
             <InformationForm/>
            </div>
          </div>
        </div>

        <br />
        <br />
      </div>
    );
  }
}
