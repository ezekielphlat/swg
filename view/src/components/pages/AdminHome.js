import React, { Component } from 'react'

import { BrowserRouter as Router, Route } from "react-router-dom";
import Dashboard from './admin/Dashboard';
import Category from './admin/Category';
import Type from './admin/Type';
import Information from './admin/Information';
import Header from './admin/Header';
import Register from './admin/Register';
import Upload from './admin/Upload';

export default class AdminHome extends Component {
    render() {
        const routes = [
            {
              path: "/",
              exact: true,
              sidebar: Dashboard,
              main: Dashboard
            },
            {
              path: "/category",
              sidebar: Category,
              main:Category
            },
            {
              path: "/type",
              sidebar:() => <span>Type!</span>,
              main:Type
            },
            {
                path: "/information",
                sidebar:() => <span>Information!</span>,
                main:Information
              },
              {
                path: "/register",
                sidebar:() => <span>Register</span>,
                main:Register
              },
              {
                path: "/upload",
                sidebar:() => <span>Upload</span>,
                main:Upload
              },
          ];
          
        return (
            <Router>
                <div>

               <Header/>

                <div id="page-wrapper">
                
                {routes.map((route, index) => (
                    // Render more <Route>s with the same paths as
                    // above, but different components this time.
                    <Route
                        key={index}
                        path={route.path}
                        exact={route.exact}
                        component={route.main}
                    />
        ))}
                </div>

               
            </div>
            </Router>
        )
    }
}
