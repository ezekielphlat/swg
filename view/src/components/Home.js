import React, { Component } from "react";
import AdminHome from "./pages/AdminHome";
import PublicHome from "./pages/PublicHome";


export default class Header extends Component {
  render() {
    const { loggedIn } = this.props;
    if (loggedIn) {
       return(<div>
             <AdminHome/>
        </div>) 
    } else {
      return (
        <div>
         <PublicHome/>
        </div>
      );
    }
  }
}
