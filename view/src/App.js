import React, { Component } from 'react';
import Home from './components/Home';


class App extends Component {
  constructor(){
    super()
    this.state= {
      loggedIn:false
    }
  }
  render() {
    return (
      <div className="App">
      <Home {...this.state} />
      </div>
    );
  }
}

export default App;
