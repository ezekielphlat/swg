package model

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

//Type model
type Type struct {
	ID         bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Name       string        `json:"name" bson:"name"`
	CategoryID bson.ObjectId `json:"categoryId" bson:"categoryId"`
	CreatedAt  time.Time     `json:"created_at" bson:"created_at"`
	UpdatedAt  time.Time     `json:"updated_at" bson:"updated_at"`
}

//Types model
type Types []Type
