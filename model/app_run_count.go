package model

import (
	"time"
)

//AppRunCount model
type AppRunCount struct {
	ID        int       `json:"id" bson:"id"`
	Count     bool      `json:"count" bson:"count"`
	CreatedAt time.Time `json:"created_at" bson:"created_at"`
	UpdatedAt time.Time `json:"updated_at" bson:"updated_at"`
}

//AppRunCounts model
type AppRunCounts []AppRunCount
