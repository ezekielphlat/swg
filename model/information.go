package model

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

//Information model
type Information struct {
	ID         bson.ObjectId `json:"id" bson:"_id,omitempty"`
	CategoryID bson.ObjectId `json:"categoryId" bson:"categoryId"`
	TypeID     bson.ObjectId `json:"typeId" bson:"typeId"`
	Info       string        `json:"info" bson:"info"`
	CreatedAt  time.Time     `json:"created_at" bson:"created_at"`
	UpdatedAt  time.Time     `json:"updated_at" bson:"updated_at"`
}

//Informations model
type Informations []Information
