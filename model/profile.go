package model

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

//Profile model
type Profile struct {
	ID        bson.ObjectId `json:"id" bson:"_id,omitempty"`
	FirstName string        `json:"first_name" bson:"first_name"`
	LastName  string        `json:"last_name" bson:"last_name"`
	Email     string        `json:"email" bson:"email"`
	Password  string        `json:"password" bson:"password"`
	GenderID  int           `json:"genderId" bson:"genderId"`
	RoleID    int           `json:"roleId" bson:"roleId"`
	CreatedAt time.Time     `json:"created_at" bson:"created_at"`
	UpdatedAt time.Time     `json:"updated_at" bson:"updated_at"`
}

//Profiles model
type Profiles []Profile
