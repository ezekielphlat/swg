package model

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

//Category model
type Category struct {
	ID        bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Name      string        `json:"name" bson:"name"`
	CreatedAt time.Time     `json:"created_at" bson:"created_at"`
	UpdatedAt time.Time     `json:"updated_at" bson:"updated_at"`
}

//Categories model
type Categories []Category
