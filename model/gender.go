package model

import (
	"time"
)

//Gender model
type Gender struct {
	ID        int       `json:"id" bson:"id"`
	Name      string    `json:"name" bson:"name"`
	CreatedAt time.Time `json:"created_at" bson:"created_at"`
	UpdatedAt time.Time `json:"updated_at" bson:"updated_at"`
}

//Genders model
type Genders []Gender

var gd = []Gender{
	Gender{
		ID:        1,
		Name:      "Male",
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	},
	Gender{
		ID:        2,
		Name:      "Female",
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	},
}

//GetGenderDate gets all initial genders for startup load
func GetGenderDate() []Gender {
	return gd
}
