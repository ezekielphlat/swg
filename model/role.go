package model

import (
	"time"
)

//Role model
type Role struct {
	ID          int       `json:"id" bson:"id"`
	Name        string    `json:"name" bson:"name"`
	Description string    `json:"description" bson:"description"`
	CreatedAt   time.Time `json:"created_at" bson:"created_at"`
	UpdatedAt   time.Time `json:"updated_at" bson:"updated_at"`
}

//Roles model
type Roles []Role

var rd = []Role{
	Role{
		ID:          1,
		Name:        "Admin",
		Description: "administrator role",
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Now(),
	},
	Role{
		ID:          2,
		Name:        "Member",
		Description: "members of the estate role",
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Now(),
	},
	Role{
		ID:          3,
		Name:        "Committee",
		Description: "committee of the estate role",
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Now(),
	},
	Role{
		ID:          4,
		Name:        "Executive",
		Description: "Executive of the estate role",
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Now(),
	},
}

//GetRoleData gets all initial data for role
func GetRoleData() []Role {
	return rd
}
