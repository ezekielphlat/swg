package model

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

//Upload structure
type Upload struct {
	ID          bson.ObjectId `json:"id" bson:"_id,omitempty"`
	CategoryID  bson.ObjectId `json:"categoryId" bson:"categoryId"`
	TypeID      bson.ObjectId `json:"typeId" bson:"typeId"`
	Title       string        `json:"title" bson:"title"`
	File        string        `json:"file" bson:"file"`
	Description string        `json:"description" bson:"description"`
	CreatedAt   time.Time     `json:"created_at" bson:"created_at"`
	UpdatedAt   time.Time     `json:"updated_at" bson:"updated_at"`
}

//Uploads structure
type Uploads []Upload
