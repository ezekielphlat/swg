**About Still Waters Garden Estate (SWG)**

####It is an Estate Information management system for a beautiful estate along Lekki-Epe Express way Lagos state.



---

## Front End

####The front end is built with react js.
---

## Back End

####The Back end is built with Golang while the database is Mongo DB.
---

## State of the Project
####Most of the backend jobs are done however the front end still needs serious attention. More content is needed from the client so as to continue the project.

