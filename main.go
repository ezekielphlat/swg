package main

import (
	"fmt"
	"log"
	"time"

	"bitbucket.org/ezekielphlat/swg/config"
	"bitbucket.org/ezekielphlat/swg/controller"
	"bitbucket.org/ezekielphlat/swg/dao"
	"bitbucket.org/ezekielphlat/swg/helper"
	"bitbucket.org/ezekielphlat/swg/repository"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	mgo "gopkg.in/mgo.v2"
)

func main() {
	//Set the router as the default one shipped with Gin
	router := gin.Default()
	_, ses := connectToDatabase()
	defer ses.Close()

	//Serve frontend static files
	// router.Use(static.Serve("/", static.LocalFile("./testview", true)))
	//allow origin
	router.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"http://localhost:3000"},
		AllowMethods:     []string{"PUT", "PATCH"},
		AllowHeaders:     []string{"Origin"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		AllowOriginFunc: func(origin string) bool {
			return origin == "https://github.com"
		},
		MaxAge: 12 * time.Hour,
	}))

	//Setup route group for the API
	controller.RegisterProfileRoutes(router)
	controller.RegisterCategoryRoutes(router)
	controller.RegisterInformationRoutes(router)
	controller.RegisterUploadRoutes(router)

	//Start and run the server

	router.Run(":8000")
}

func connectToDatabase() (*mgo.Database, *mgo.Session) {
	db, session, err := config.GetMongoDB()

	if err != nil {
		fmt.Println(err)
	}
	controller.SetDatabase(db)
	return db, session
}

func init() {
	db, session := connectToDatabase()
	defer session.Close()

	genderRepository := repository.NewGenderRepositoryMongo(db, helper.GenderCollection)
	roleRepository := repository.NewRoleRepositoryMongo(db, helper.RoleCollection)
	arcRepository := repository.NewAppRunCountRepositoryMongo(db, helper.ArcCollection)

	// findGender(genderRepository)
	if !dao.HasAppRunned(arcRepository) {
		dao.SaveRole(roleRepository)
		dao.SaveGender(genderRepository)
		dao.SetAppRunCount(arcRepository)
		log.Println("Application data successfully inserted")
	} else {
		log.Println("App has ran before")
	}

}
