package helper

const (
	//ProfileCollection database collection name
	ProfileCollection = "profile"
	//GenderCollection database collection name
	GenderCollection = "gender"
	//RoleCollection database collection name
	RoleCollection = "role"
	//ArcCollection database collection name
	ArcCollection = "arCount"
	//CategoryCollection database collection name
	CategoryCollection = "category"
	//TypeCollection database collection name
	TypeCollection = "type"
	//InformationCollection database collection name
	InformationCollection = "information"
	//UploadCollection database collection name
	UploadCollection = "upload"
)

//Store is a session store
// var Store = sessions.NewCookieStore([]byte("secret-password"))
