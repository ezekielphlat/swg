package helper

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

// func CheckSessionValue(w http.ResponseWriter, r *http.Request) string {
// 	session, _ := Store.Get(r, "session")
// 	if email, ok := session.Values["email"]; ok {
// 		strs := strings.Split(email.(string), "@")
// 		// log.Println(strs)

// 		vm.User = strings.Title(strs[0])

// 	} else {
// 		http.Error(w, "Forbidden", http.StatusForbidden)
// 		return
// 	}

// }

//PrintResponse is a global error log function
func PrintResponse(err error, c *gin.Context, action string, collection string) {
	if err != nil {
		log.Println(fmt.Errorf("error %v new user: %v", action, err))
		c.JSON(http.StatusOK, gin.H{
			"status":  0,
			"message": collection + " successfully saves",
		})

	} else {
		log.Println("new " + collection + " successfully saves")
		c.JSON(http.StatusOK, gin.H{
			"status":  1,
			"message": collection + " successfully saves",
		})
	}
}
