package controller

import (
	"net/http"
	"time"

	"bitbucket.org/ezekielphlat/swg/dao"
	"bitbucket.org/ezekielphlat/swg/helper"
	"bitbucket.org/ezekielphlat/swg/model"
	"bitbucket.org/ezekielphlat/swg/repository"
	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

//RegisterInformationRoutes registers all the routes in profile to the main function
func RegisterInformationRoutes(r *gin.Engine) {
	categoryAPI := r.Group("/api/information")
	categoryAPI.GET("/allinfo", getAllInformations)
	categoryAPI.POST("/addinfo", addNewInformation)

}

func getAllInformations(c *gin.Context) {
	informationRepository := repository.NewInformationRepositoryMongo(db, helper.InformationCollection)

	infos := dao.FindInformations(informationRepository)

	c.IndentedJSON(http.StatusOK, infos)
}
func addNewInformation(c *gin.Context) {
	informationRepository := repository.NewInformationRepositoryMongo(db, helper.InformationCollection)

	newInformation := &model.Information{

		CategoryID: bson.ObjectIdHex(c.PostForm("category")),
		TypeID:     bson.ObjectIdHex(c.PostForm("type")),
		Info:       c.PostForm("info"),
		CreatedAt:  time.Now(),
		UpdatedAt:  time.Now(),
	}
	_, err := dao.SaveInformation(informationRepository, newInformation)
	helper.PrintResponse(err, c, "Saving", "Information")
}
