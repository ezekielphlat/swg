package controller

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"bitbucket.org/ezekielphlat/swg/dao"
	"bitbucket.org/ezekielphlat/swg/helper"
	"bitbucket.org/ezekielphlat/swg/model"
	"bitbucket.org/ezekielphlat/swg/repository"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gopkg.in/mgo.v2/bson"
)

//RegisterUploadRoutes registers all the routes in profile to the main function
func RegisterUploadRoutes(r *gin.Engine) {
	categoryAPI := r.Group("/api/upload")
	categoryAPI.GET("/all", getAllUploads)
	categoryAPI.POST("/addnew", addNewUpload)

}

func getAllUploads(c *gin.Context) {
	uploadRepository := repository.NewUploadRepositoryMongo(db, helper.UploadCollection)

	uploads := dao.FindUploads(uploadRepository)

	c.IndentedJSON(http.StatusOK, uploads)
}

func addNewUpload(c *gin.Context) {
	uploadRepository := repository.NewUploadRepositoryMongo(db, helper.UploadCollection)
	typeRepository := repository.NewTypeRepositoryMongo(db, helper.TypeCollection)

	// Source
	file, err := c.FormFile("file")
	if err != nil {
		c.String(http.StatusBadRequest, fmt.Sprintf("get form err: %s", err.Error()))
		return
	}
	typeName := dao.FindTypeById(typeRepository, bson.ObjectIdHex(c.PostForm("type")))

	filepath := os.Getenv("SWG_UPLOAD_PATH") + "/uploads/"
	splittedString := strings.Split(file.Header["Content-Type"][0], "/")
	if splittedString[0] == "image" {

		filepath += "images/" + typeName.Name + "/"
		fmt.Println(filepath)
	} else {
		filepath += "documents/" + typeName.Name + "/"
		fmt.Println(filepath)
	}

	errr := os.MkdirAll(filepath, os.ModePerm)
	if err != nil {
		fmt.Println(errr)
	}

	if err := c.SaveUploadedFile(file, filepath+uuid.New().String()+file.Filename); err != nil {
		c.String(http.StatusBadRequest, fmt.Sprintf("upload file err: %s", err.Error()))
		log.Println("cannot upload ", err)
		return
	}

	c.String(http.StatusOK, fmt.Sprintf("File %s uploaded successfully with fields.", file.Filename))
	newUpload := &model.Upload{

		CategoryID:  bson.ObjectIdHex(c.PostForm("category")),
		TypeID:      bson.ObjectIdHex(c.PostForm("type")),
		Title:       c.PostForm("title"),
		File:        file.Filename,
		Description: c.PostForm("description"),
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Now(),
	}

	_, saveerr := dao.SaveUpload(uploadRepository, newUpload)
	helper.PrintResponse(saveerr, c, "Uploading", "Upload")

}
