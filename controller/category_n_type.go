package controller

import (
	"net/http"
	"time"

	"bitbucket.org/ezekielphlat/swg/dao"
	"bitbucket.org/ezekielphlat/swg/helper"
	"bitbucket.org/ezekielphlat/swg/model"
	"gopkg.in/mgo.v2/bson"

	"bitbucket.org/ezekielphlat/swg/repository"
	"github.com/gin-gonic/gin"
)

//RegisterCategoryRoutes registers all the routes in profile to the main function
func RegisterCategoryRoutes(r *gin.Engine) {
	categoryAPI := r.Group("/api/category_n_type")
	categoryAPI.GET("/categories", handleCategories)
	categoryAPI.POST("/category", handleCategory)
	categoryAPI.GET("/types", handleTypes)
	categoryAPI.GET("/types/:id", handleTypesByID)
	categoryAPI.POST("/type", handleType)

}

func handleCategories(c *gin.Context) {
	categoryRepository := repository.NewCategoryRepositoryMongo(db, helper.CategoryCollection)

	categories := dao.FindCategories(categoryRepository)
	c.IndentedJSON(http.StatusOK, categories)
}
func handleCategory(c *gin.Context) {
	categoryRepository := repository.NewCategoryRepositoryMongo(db, helper.CategoryCollection)
	newCategory := &model.Category{

		Name:      c.PostForm("name"),
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
	_, err := dao.SaveCategory(categoryRepository, newCategory)
	helper.PrintResponse(err, c, "Saving", "Category")

}

func handleTypes(c *gin.Context) {
	typeRepository := repository.NewTypeRepositoryMongo(db, helper.TypeCollection)
	types := dao.FindTypes(typeRepository)
	c.IndentedJSON(http.StatusOK, types)

}

func handleType(c *gin.Context) {
	typeRepository := repository.NewTypeRepositoryMongo(db, helper.TypeCollection)
	newType := &model.Type{

		Name:       c.PostForm("name"),
		CategoryID: bson.ObjectIdHex(c.PostForm("category")),
		CreatedAt:  time.Now(),
		UpdatedAt:  time.Now(),
	}
	_, err := dao.SaveType(typeRepository, newType)
	helper.PrintResponse(err, c, "Saving", "Type")

}
func handleTypesByID(c *gin.Context) {
	typeRepository := repository.NewTypeRepositoryMongo(db, helper.TypeCollection)
	typeID := bson.ObjectIdHex(c.Param("id"))
	types := dao.FindTypesByCategoryId(typeRepository, typeID)
	c.IndentedJSON(http.StatusOK, types)

}
