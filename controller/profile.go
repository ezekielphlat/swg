package controller

import (
	"log"
	"net/http"
	"strconv"
	"time"

	"bitbucket.org/ezekielphlat/swg/dao"
	"bitbucket.org/ezekielphlat/swg/helper"
	"bitbucket.org/ezekielphlat/swg/model"
	"bitbucket.org/ezekielphlat/swg/repository"
	"github.com/gin-gonic/gin"
)

//RegisterProfileRoutes registers all the routes in profile to the main function
func RegisterProfileRoutes(r *gin.Engine) {
	profileAPI := r.Group("/api/profile")
	profileAPI.GET("/profiles", handleProfiles)
	profileAPI.GET("/genders", handleGenders)
	profileAPI.GET("/roles", handleRoles)
	profileAPI.POST("/login", handleLogin)
	profileAPI.POST("/register", handleRegister)

}
func handleProfiles(c *gin.Context) {
	profileRepository := repository.NewProfileRepositoryMongo(db, helper.ProfileCollection)

	users := dao.FindProfiles(profileRepository)
	c.IndentedJSON(http.StatusOK, users)

}

func handleGenders(c *gin.Context) {
	genderRepository := repository.NewGenderRepositoryMongo(db, helper.GenderCollection)

	genders := dao.FindGenders(genderRepository)
	c.IndentedJSON(http.StatusOK, genders)
	// vm.Genders = genders
}
func handleRoles(c *gin.Context) {
	roleRepository := repository.NewRoleRepositoryMongo(db, helper.RoleCollection)

	roles := dao.FindRoles(roleRepository)
	c.IndentedJSON(http.StatusOK, roles)
	// vm.Roles = roles
}

func handleLogin(c *gin.Context) {

}

func handleRegister(c *gin.Context) {
	profileRepository := repository.NewProfileRepositoryMongo(db, helper.ProfileCollection)

	roleID, er := strconv.Atoi(c.PostForm("role"))
	if er != nil {
		log.Println(er)
	}
	genderID, _ := strconv.Atoi(c.PostForm("gender"))
	newUser := &model.Profile{

		FirstName: c.PostForm("firstname"),
		LastName:  c.PostForm("lastname"),
		RoleID:    roleID,
		GenderID:  genderID,
		Email:     c.PostForm("email"),
		Password:  c.PostForm("password"),
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
	_, err := dao.SaveProfile(profileRepository, newUser)
	helper.PrintResponse(err, c, "Registering", "Profile")

}
