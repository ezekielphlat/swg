package controller

import mgo "gopkg.in/mgo.v2"

var db *mgo.Database

func SetDatabase(database *mgo.Database) {
	db = database
}
