package repository

import (
	"gopkg.in/mgo.v2/bson"

	"bitbucket.org/ezekielphlat/swg/model"
)

//ProfileRepository is a an interface type
type ProfileRepository interface {
	Save(*model.Profile) error
	Update(string, *model.Profile) error
	Delete(string) error
	FindByEmail(string) (*model.Profile, error)
	FindByID(string) (*model.Profile, error)
	FindAll() (model.Profiles, error)
}

//GenderRepository is a an interface type
type GenderRepository interface {
	Save(*model.Gender) error
	FindByID(string) (*model.Gender, error)
	FindAll() (model.Genders, error)
}

//RoleRepository is a an interface type
type RoleRepository interface {
	Save(*model.Role) error
	Update(string, *model.Role) error
	Delete(string) error
	FindByID(string) (*model.Role, error)
	FindAll() (model.Roles, error)
}

//AppRunCountRepository is a an interface type
type AppRunCountRepository interface {
	Save(*model.AppRunCount) error
	Update(string, *model.AppRunCount) error
	Delete(string) error
	FindByID(string) (*model.AppRunCount, error)
	FindAll() (model.AppRunCounts, error)
}

//CategoryRepository is a an interface type
type CategoryRepository interface {
	Save(*model.Category) error
	Update(string, *model.Category) error
	Delete(string) error
	FindByID(string) (*model.Category, error)
	FindAll() (model.Categories, error)
}

//TypeRepository is a an interface type
type TypeRepository interface {
	Save(*model.Type) error
	Update(string, *model.Type) error
	Delete(string) error
	FindByID(bson.ObjectId) (*model.Type, error)
	FindAll() (model.Types, error)
	FindAllByCatID(bson.ObjectId) (model.Types, error)
}

//InformationRepository is a an interface type
type InformationRepository interface {
	Save(*model.Information) error
	Update(string, *model.Information) error
	Delete(string) error
	FindByID(string) (*model.Information, error)
	FindAll() (model.Informations, error)
}

//UploadRepository is a an interface type
type UploadRepository interface {
	Save(*model.Upload) error
	Update(string, *model.Upload) error
	Delete(string) error
	FindByID(string) (*model.Upload, error)
	FindAll() (model.Uploads, error)
}
