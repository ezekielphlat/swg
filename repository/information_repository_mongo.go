package repository

import (
	"html"
	"time"

	"bitbucket.org/ezekielphlat/swg/model"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type informationRepositoryMongo struct {
	db         *mgo.Database
	collection string
}

//NewCategoryRepositoryMongo is used
func NewInformationRepositoryMongo(db *mgo.Database, collection string) *informationRepositoryMongo {
	return &informationRepositoryMongo{
		db:         db,
		collection: collection,
	}
}

func (i *informationRepositoryMongo) Save(info *model.Information) error {
	information := info.Info
	escapedInfo := html.EscapeString(information)
	info.Info = escapedInfo

	err := i.db.C(i.collection).Insert(info)
	return err
}

func (i *informationRepositoryMongo) Update(id string, info *model.Information) error {
	info.UpdatedAt = time.Now()
	err := i.db.C(i.collection).Update(bson.M{"id": id}, info)
	return err
}

func (i *informationRepositoryMongo) Delete(id string) error {

	err := i.db.C(i.collection).Remove(bson.M{"id": id})
	return err
}

func (i *informationRepositoryMongo) FindByID(id string) (*model.Information, error) {
	var info model.Information
	err := i.db.C(i.collection).Find(bson.M{"id": id}).One(&info)
	if err != nil {
		return nil, err
	}

	return &info, nil
}

func (i *informationRepositoryMongo) FindAll() (model.Informations, error) {
	var infos model.Informations
	err := i.db.C(i.collection).Find(bson.M{}).All(&infos)
	if err != nil {
		return nil, err
	}

	return infos, nil
}
