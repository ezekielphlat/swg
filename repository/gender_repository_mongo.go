package repository

import (
	"bitbucket.org/ezekielphlat/swg/model"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type genderRepositoryMongo struct {
	db         *mgo.Database
	collection string
}

//NewGenderRepositoryMongo is used
func NewGenderRepositoryMongo(db *mgo.Database, collection string) *genderRepositoryMongo {
	return &genderRepositoryMongo{
		db:         db,
		collection: collection,
	}
}

func (g *genderRepositoryMongo) Save(gender *model.Gender) error {
	err := g.db.C(g.collection).Insert(gender)
	return err
}

func (g *genderRepositoryMongo) FindByID(id string) (*model.Gender, error) {
	var gender model.Gender
	err := g.db.C(g.collection).Find(bson.M{"id": id}).One(&gender)
	if err != nil {
		return nil, err
	}

	return &gender, nil
}

func (g *genderRepositoryMongo) FindAll() (model.Genders, error) {
	var genders model.Genders
	err := g.db.C(g.collection).Find(bson.M{}).All(&genders)
	if err != nil {
		return nil, err
	}
	return genders, nil
}
