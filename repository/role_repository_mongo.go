package repository

import (
	"time"

	"bitbucket.org/ezekielphlat/swg/model"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type roleRepositoryMongo struct {
	db         *mgo.Database
	collection string
}

//NewRoleRepositoryMongo is used
func NewRoleRepositoryMongo(db *mgo.Database, collection string) *roleRepositoryMongo {
	return &roleRepositoryMongo{
		db:         db,
		collection: collection,
	}
}

func (r *roleRepositoryMongo) Save(role *model.Role) error {
	err := r.db.C(r.collection).Insert(role)
	return err
}

func (r *roleRepositoryMongo) Update(id string, role *model.Role) error {
	role.UpdatedAt = time.Now()
	err := r.db.C(r.collection).Update(bson.M{"id": id}, role)
	return err
}

func (r *roleRepositoryMongo) FindAll() (model.Roles, error) {
	var roles model.Roles
	err := r.db.C(r.collection).Find(bson.M{}).All(&roles)
	if err != nil {
		return nil, err
	}
	return roles, nil
}

func (r *roleRepositoryMongo) FindByID(id string) (*model.Role, error) {
	var role model.Role
	err := r.db.C(r.collection).Find(bson.M{"id": id}).One(&role)
	if err != nil {
		return nil, err
	}

	return &role, nil
}

func (r *roleRepositoryMongo) Delete(id string) error {

	err := r.db.C(r.collection).Remove(bson.M{"id": id})
	return err
}
