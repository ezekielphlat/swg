package repository

import (
	"time"

	"bitbucket.org/ezekielphlat/swg/model"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type appRunCountRepositoryMongo struct {
	db         *mgo.Database
	collection string
}

//NewAppRunCountRepositoryMongo is used
func NewAppRunCountRepositoryMongo(db *mgo.Database, collection string) *appRunCountRepositoryMongo {
	return &appRunCountRepositoryMongo{
		db:         db,
		collection: collection,
	}
}

func (a *appRunCountRepositoryMongo) Save(arc *model.AppRunCount) error {
	err := a.db.C(a.collection).Insert(arc)
	return err
}

func (a *appRunCountRepositoryMongo) Update(id string, arc *model.AppRunCount) error {
	arc.UpdatedAt = time.Now()
	err := a.db.C(a.collection).Update(bson.M{"id": id}, arc)
	return err
}

func (a *appRunCountRepositoryMongo) Delete(id string) error {

	err := a.db.C(a.collection).Remove(bson.M{"id": id})
	return err
}

func (a *appRunCountRepositoryMongo) FindByID(id string) (*model.AppRunCount, error) {
	var arc model.AppRunCount
	err := a.db.C(a.collection).Find(bson.M{"id": id}).One(&arc)
	if err != nil {
		return nil, err
	}

	return &arc, nil
}

func (a *appRunCountRepositoryMongo) FindAll() (model.AppRunCounts, error) {
	var arcs model.AppRunCounts
	err := a.db.C(a.collection).Find(bson.M{}).All(&arcs)
	if err != nil {
		return nil, err
	}
	return arcs, nil
}
