package repository

import (
	"time"

	"bitbucket.org/ezekielphlat/swg/model"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type typeRepositoryMongo struct {
	db         *mgo.Database
	collection string
}

//NewTypeRepositoryMongo is used
func NewTypeRepositoryMongo(db *mgo.Database, collection string) *typeRepositoryMongo {
	return &typeRepositoryMongo{
		db:         db,
		collection: collection,
	}
}

func (t *typeRepositoryMongo) Save(cattype *model.Type) error {
	err := t.db.C(t.collection).Insert(cattype)
	return err
}

func (t *typeRepositoryMongo) Update(id string, cattype *model.Type) error {
	cattype.UpdatedAt = time.Now()
	err := t.db.C(t.collection).Update(bson.M{"id": id}, cattype)
	return err
}

func (t *typeRepositoryMongo) Delete(id string) error {

	err := t.db.C(t.collection).Remove(bson.M{"id": id})
	return err
}

func (t *typeRepositoryMongo) FindByID(id bson.ObjectId) (*model.Type, error) {
	var cattype model.Type
	err := t.db.C(t.collection).Find(bson.M{"_id": id}).One(&cattype)
	if err != nil {
		return nil, err
	}

	return &cattype, nil
}

func (t *typeRepositoryMongo) FindAll() (model.Types, error) {
	var catypes model.Types
	err := t.db.C(t.collection).Find(bson.M{}).All(&catypes)
	if err != nil {
		return nil, err
	}
	return catypes, nil
}

func (t *typeRepositoryMongo) FindAllByCatID(id bson.ObjectId) (model.Types, error) {
	var catypes model.Types
	err := t.db.C(t.collection).Find(bson.M{"categoryId": id}).All(&catypes)
	if err != nil {
		return nil, err
	}
	return catypes, nil
}
