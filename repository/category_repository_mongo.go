package repository

import (
	"time"

	"bitbucket.org/ezekielphlat/swg/model"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type categoryRepositoryMongo struct {
	db         *mgo.Database
	collection string
}

//NewCategoryRepositoryMongo is used
func NewCategoryRepositoryMongo(db *mgo.Database, collection string) *categoryRepositoryMongo {
	return &categoryRepositoryMongo{
		db:         db,
		collection: collection,
	}
}

func (c *categoryRepositoryMongo) Save(category *model.Category) error {
	err := c.db.C(c.collection).Insert(category)
	return err
}

func (c *categoryRepositoryMongo) Update(id string, category *model.Category) error {
	category.UpdatedAt = time.Now()
	err := c.db.C(c.collection).Update(bson.M{"id": id}, category)
	return err
}

func (c *categoryRepositoryMongo) Delete(id string) error {

	err := c.db.C(c.collection).Remove(bson.M{"id": id})
	return err
}

func (c *categoryRepositoryMongo) FindByID(id string) (*model.Category, error) {
	var category model.Category
	err := c.db.C(c.collection).Find(bson.M{"id": id}).One(&category)
	if err != nil {
		return nil, err
	}

	return &category, nil
}

func (c *categoryRepositoryMongo) FindAll() (model.Categories, error) {
	var categories model.Categories
	err := c.db.C(c.collection).Find(bson.M{}).All(&categories)
	if err != nil {
		return nil, err
	}

	return categories, nil
}
