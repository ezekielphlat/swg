package repository

import (
	"time"

	"bitbucket.org/ezekielphlat/swg/model"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type uploadRepositoryMongo struct {
	db         *mgo.Database
	collection string
}

//NewUploadRepositoryMongo is used
func NewUploadRepositoryMongo(db *mgo.Database, collection string) *uploadRepositoryMongo {
	return &uploadRepositoryMongo{
		db:         db,
		collection: collection,
	}
}

func (u *uploadRepositoryMongo) Save(file *model.Upload) error {
	err := u.db.C(u.collection).Insert(file)
	return err
}

func (u *uploadRepositoryMongo) Update(id string, file *model.Upload) error {
	file.UpdatedAt = time.Now()
	err := u.db.C(u.collection).Update(bson.M{"id": id}, file)
	return err
}

func (u *uploadRepositoryMongo) FindAll() (model.Uploads, error) {
	var files model.Uploads
	err := u.db.C(u.collection).Find(bson.M{}).All(&files)
	if err != nil {
		return nil, err
	}
	return files, nil
}

func (u *uploadRepositoryMongo) FindByID(id string) (*model.Upload, error) {
	var file model.Upload
	err := u.db.C(u.collection).Find(bson.M{"id": id}).One(&file)
	if err != nil {
		return nil, err
	}

	return &file, nil
}

func (u *uploadRepositoryMongo) Delete(id string) error {

	err := u.db.C(u.collection).Remove(bson.M{"id": id})
	return err
}
